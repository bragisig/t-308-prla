usage: whoispopular.py [-h] [-n numberofmovies] [-y afteryear] [-d displaytop]

Get the most popular actors from top movies on www.imdb.com

optional arguments:
  -h, --help         show this help message and exit
  -n numberofmovies  Results should only be from top n movies
  -y afteryear       Results should only be from movies produced on or after
                     year y
  -d displaytop      Only display top d results

  Examples:
  
  Get most popular actors in the top 20 movies, only show top 10 actors:

  		python3 whoispopular.py -n 20 -d 10
  
  Get most popular actors in the top 20 movies, only show top 10 actors and only show movies after 1995:
  	
  		python3 whoispopular.py -n 20 -d 10 -y 1995

  Get most popular actors in the top 250 movies, only show top 3 actors 

		whoispopular.py -n 250 -d 10

		Output:

		 1 Bess Flowers............................ movies: 12
		    - The Manchurian Candidate (1962)
		    - North by Northwest (1959)
		    - Vertigo (1958)
		    - Witness for the Prosecution (1957)
		    - Rear Window (1954)
		    - Dial M for Murder (1954)
		    - Singin' in the Rain (1952)
		    - All About Eve (1950)
		    - Notorious (1946)
		    - The Big Sleep (1946)
		    - Double Indemnity (1944)
		    - It Happened One Night (1934)
		  2 John Ratzenberger....................... movies: 10
		    - Toy Story 3 (2010)
		    - Up (2009)
		    - WALL·E (2008)
		    - Ratatouille (2007)
		    - Finding Nemo (2003)
		    - Spirited Away (2001)
		    - Monsters, Inc. (2001)
		    - Toy Story (1995)
		    - Gandhi (1982)
		    - Star Wars: Episode V - The Empire Strikes Back (1980)
		  3 Alfred Hitchcock........................ movies:  9
		    - Psycho (1960)
		    - North by Northwest (1959)
		    - Vertigo (1958)
		    - Rear Window (1954)
		    - Dial M for Murder (1954)
		    - Strangers on a Train (1951)
		    - Rope (1948)
		    - Notorious (1946)
		    - Rebecca (1940)
		  4 James Stewart........................... movies:  9
		    - Cinema Paradiso (1988)
		    - The Man Who Shot Liberty Valance (1962)
		    - Anatomy of a Murder (1959)
		    - Vertigo (1958)
		    - Rear Window (1954)
		    - Harvey (1950)
		    - Rope (1948)
		    - It's a Wonderful Life (1946)
		    - Mr. Smith Goes to Washington (1939)
		  5 Robert De Niro.......................... movies:  9
		    - Heat (1995)
		    - Casino (1995)
		    - Goodfellas (1990)
		    - The Untouchables (1987)
		    - Once Upon a Time in America (1984)
		    - Raging Bull (1980)
		    - The Deer Hunter (1978)
		    - Taxi Driver (1976)
		    - The Godfather: Part II (1974)
		  6 Sherry Lynn............................. movies:  9
		    - Toy Story 3 (2010)
		    - Up (2009)
		    - WALL·E (2008)
		    - Oldboy (2003)
		    - Spirited Away (2001)
		    - Monsters, Inc. (2001)
		    - Princess Mononoke (1997)
		    - Toy Story (1995)
		    - Beauty and the Beast (1991)
		  7 Gino Corrado............................ movies:  8
		    - Harvey (1950)
		    - Casablanca (1942)
		    - Citizen Kane (1941)
		    - The Great Dictator (1940)
		    - Rebecca (1940)
		    - The Grapes of Wrath (1940)
		    - Mr. Smith Goes to Washington (1939)
		    - Gone with the Wind (1939)
		  8 Harrison Ford........................... movies:  7
		    - Indiana Jones and the Last Crusade (1989)
		    - Star Wars: Episode VI - Return of the Jedi (1983)
		    - Blade Runner (1982)
		    - Raiders of the Lost Ark (1981)
		    - Star Wars: Episode V - The Empire Strikes Back (1980)
		    - Apocalypse Now (1979)
		    - Star Wars (1977)
		  9 Mickie McGowan.......................... movies:  7
		    - Toy Story 3 (2010)
		    - Up (2009)
		    - WALL·E (2008)
		    - Spirited Away (2001)
		    - Monsters, Inc. (2001)
		    - Toy Story (1995)
		    - Beauty and the Beast (1991)
		 10 Morgan Freeman.......................... movies:  7
		    - The Dark Knight Rises (2012)
		    - The Dark Knight (2008)
		    - Batman Begins (2005)
		    - Million Dollar Baby (2004)
		    - Se7en (1995)
		    - The Shawshank Redemption (1994)
		    - Unforgiven (1992)
