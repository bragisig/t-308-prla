import getpass
import requests
import bs4
from requests.auth import HTTPBasicAuth
import argparse

parser = argparse.ArgumentParser(description='Skila verkefni í myschool.')
parser.add_argument('username', metavar='username', type=str, 
                   help='Myschool notandanafnið þitt')
parser.add_argument('comment', metavar='comment', type=str, 
                   help='Skilaboð frá nemanda')
parser.add_argument('filename', metavar='filename', type=str, 
                   help='Skrá til að skila inn')
parser.add_argument('url', metavar='url', type=str, 
                   help='Slóð í Myschool til að pósta á')

args  = parser.parse_args()

def myschool_submit(username, password, comment, file1, url):
	payload = { 'athugasemdnemanda': comment.encode('ISO-8859-1') }
	files={ 'file': (file1, open(file1, 'rb')) }
	
	try:
		r = requests.post(url, auth=HTTPBasicAuth(username, password), data=payload, files=files)

		r.raise_for_status()

		print('Það gekk að skila verkefninu!')
	except Error as ex:
		print('Villa kom upp við að reyna skila verkefninu, villan:', ex) 

myschool_submit(args.username, getpass.getpass(), args.comment, args.filename, args.url)