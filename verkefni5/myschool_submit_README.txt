usage: myschool_submit.py [-h] username comment filename url

Skila verkefni í myschool.

positional arguments:
  username    Myschool notandanafnið þitt
  comment     Skilaboð frá nemanda
  filename    Skrá til að skila inn
  url         Slóð í Myschool til að pósta á

optional arguments:
  -h, --help  show this help message and exit

Dæmi:

python3 myschool_submit.py bragifs05 "Kláraði dæmi 1" "test.zip" "https://myschool.ru.is/myschool/?Page=LMS&ID=16&fagID=25602&View=&ViewMode=2&Tab=&Act=11&verkID=41566"