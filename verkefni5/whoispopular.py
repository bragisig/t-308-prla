import requests
import bs4, re
import argparse

parser = argparse.ArgumentParser(description='Get the most popular actors from top movies on www.imdb.com')

parser.add_argument('-n', metavar='numberofmovies', type=int, help='Results should only be from top n movies', default=3, choices=range(1, 250+1))
parser.add_argument('-y', metavar='afteryear', type=int, help='Results should only be from movies produced on or after year y', choices=range(1900, 2100+1))
parser.add_argument('-d', metavar='displaytop', type=int, help='Only display top d results', default=10, choices=range(1, 1000+1))

args  = parser.parse_args()

def get_movie_list(n_movies, after_year):
	print('Scraping www.imdb.com, be patient... ')
	url = 'http://www.imdb.com'
	r = requests.get(url + '/chart/top')
	html = bs4.BeautifulSoup(r.text)
	table = html.find('div', id='main').find_all('table')[1]

	counter = 0
	movies = []
	for row in table.find_all('tr')[1:]:
		if counter == n_movies:
			break
		counter += 1

		cols = row.find_all()
		m = []
		m.append(cols[0].text)
		m.append(cols[3].text)
		m.append(cols[5].text)
		m.append(cols[8].text)
		match = re.findall('\d{4}', cols[5].text)
		year = int(match[0])
		m.append(year)
		movie_link = row.find('a').get('href')
		
		if after_year != None and year < after_year:
			continue 

		movie_link += 'fullcredits?ref_=tt_cl_sm#cast'

		movie_req = requests.get(url+movie_link)
		movie_html = bs4.BeautifulSoup(movie_req.text)
		actors = movie_html.find('table', class_='cast').find_all('td', class_='nm')

		a = []
		for actor in actors:
			a.append(actor.text)

		m.append(a)
		movies.append(m)

	return movies	

def whoispopular(n_movies, after_year):
	movies = get_movie_list(n_movies, after_year)

	actor_movie_count = {}
	for movie in movies:
		for actor in movie[5]:
			if actor in actor_movie_count:
				actor_movie_count[actor].append([movie[2],movie[4]])
			else:
				actor_movie_count[actor] = [[movie[2],movie[4]]]	


	l = actor_movie_count.items()
	ls = sorted(l, key=lambda x: x[0])
	ls = sorted(ls, key=lambda x: len(x[1]), reverse=True)

	return ls

popular_actors = whoispopular(args.n, args.y)

for i in range(args.d):
	print(repr(i+1).rjust(3), popular_actors[i][0].ljust(40, '.'), 'movies:' , repr(len(popular_actors[i][1])).rjust(2))
	popular_actors[i][1].sort(key=lambda x: x[1], reverse=True)
	for m in popular_actors[i][1]:
		print('    -', m[0])

