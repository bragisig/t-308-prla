import subprocess
from flask import Flask, request, render_template
from werkzeug import secure_filename
import os, sys

app = Flask(__name__)

COMPILER_LOCATION = '/usr/bin/g++'

@app.route("/")
def index():
    return render_template('compile.html')

@app.route("/compile", methods=['POST'])
def postcpp():
	#try:
		f = request.files['codefile']
		fn = secure_filename(f.filename)
		f.save(fn)
		fn_without_ext, ext = os.path.splitext(fn)

		std_input = request.form['std_input']
		print(std_input)
		p1 = subprocess.Popen([COMPILER_LOCATION, fn, '-o', fn_without_ext], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		c = p1.communicate()
		output=c[0].decode('utf8')
		error=c[1].decode('utf8') 

		if error == '':
			here = os.path.dirname(__file__)
			fn_without_ext = os.path.join(here, fn_without_ext)
			print(fn_without_ext)
			p2 = subprocess.Popen([fn_without_ext], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
			r = p2.communicate(input=bytes(std_input, 'utf8'))
			output=r[0].decode('utf8')
			error=r[1].decode('utf8')
		
		return render_template('results.html', output=output or '[No output]', error=error or '[No errors]')
	#except:
	#	return render_template('results.html', error="Unexpected error: " + str(sys.exc_info()[0]))


if __name__ == "__main__":
    app.run(debug=True)