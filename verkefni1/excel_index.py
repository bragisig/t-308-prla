def excel_index(s):
	x = 0

	for i, c in enumerate(s[::-1]):
		x += (ord(c)-64) * 26**i

	return x