def palindrome(n, b):
	s = convert(n,b)

	print(s)
	print(s[::-1])

	if s == s[::-1]: 
		return True

	return False

def convert(number,base):
	add = number%base
	if number<=1:
		return str(number)
	else:
		return str(convert(number//base,base)) + str(add)
