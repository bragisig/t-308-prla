def birthdays(s):
    l = [n.strip() for n in s.splitlines()]
    
    dict = {}

    for n in l:
        i = n[:4] 
        if i in dict:
            dict[i] += (n,)
        else:
            dict[i] = (n,) 

    r = []
    for k in dict:
        if len(dict[k]) > 1:
            r.append(dict[k])

    return r

