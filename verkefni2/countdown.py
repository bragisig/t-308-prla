from itertools import permutations	

def countdown(pathToFile, str):
	d = file_to_dict(pathToFile)
	perm = list(permutations(str))
	
	result = {}

	number_of_chars = min(len(str), 4)
	while number_of_chars <= len(str):		
		for p in perm:
			test_word = ''.join(e for e in p)[:number_of_chars]
			if test_word in d and test	_word not in result:
				result[test_word] = test_word

		number_of_chars += 1

	result_list = [v for k,v in result.items()]
	result_list.sort()

	return result_list

def file_to_dict(pathToFile):
	d = {}
	with open(pathToFile, 'r') as f:
		for word in f:
			w = word.strip()
			d[w] = w

	return d

