def process_ls(output):
	l = [" ".join(s.split()).split(' ', 8) for s in output.splitlines() if s[0] != "d"]
	sorted_list = sorted(l, key=lambda  x: x[8])
	sorted_list = sorted(sorted_list, key=lambda  x: float(x[4]), reverse=True)
	return list([l[8] for l in sorted_list])
