import itertools

card_ranks = { '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14 }

def rank_hand(hand):
	def check_flush(colors):
		for c1, c2 in zip(colors,colors[1:]):
			if c1 != c2:
				return False

		return True

	def check_straight(ranks):
		for r1, r2 in zip(ranks, ranks[1:]):
			if r2 - r1 != 1:
				return False

		return True


	def get_ranks_and_colors(hand):
		r = []
		c = []

		for card in hand:
			r.append(card_ranks[card[0]])
			c.append(card[1])

		return r, c

	def full_house(groups):
		if len(groups) == 2 and (len(groups[0]) == 3 or len(groups[1]) == 3):
			return True

		return False


	def four_of_a_kind(groups):
		if len(groups) == 2 and (len(groups[0]) == 4 or len(groups[1]) == 4):
			return True

		return False

	def three_of_a_kind(groups):
		for g in groups:
			if len(g) == 3:
				return True

		return False

	def two_pairs(groups):
		counter = 0
		for g in groups:
			if len(g) == 2:
				counter += 1

		if counter == 2:
			return True
		
		return False

	def pair(groups):
		for g in groups:
			if len(g) == 2:
				return True
		
		return False


	r, c = get_ranks_and_colors(hand)
	r.sort()
	print(r)
	print(c)

	flush = check_flush(c)
	straight = check_straight(r)
	groups = [list(g) for k, g in itertools.groupby(r)]

	print('groups: ', groups, len(groups))
	print('flush: ', flush)
	print('straight: ', straight)

	if flush and straight and r[4] == 14:
		return 9
	elif flush and straight:
		return 8
	elif four_of_a_kind(groups):
		return 7
	elif full_house(groups):
		return 6
	elif flush:
		return 5
	elif straight:
		return 4
	elif three_of_a_kind(groups):
		return 3
	elif two_pairs(groups):
		return 2
	elif pair(groups):
		return 1
	else:
		return 0

	