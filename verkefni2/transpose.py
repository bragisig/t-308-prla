
def transpose(arr):
	return [list(i) for i in zip(*arr)]