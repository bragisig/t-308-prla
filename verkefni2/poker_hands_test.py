from poker_hands import rank_hand

print(rank_hand([ '3D', '2H', '3C', 'QS', '8D' ]))
#1
print(rank_hand([ 'KD', 'KH', 'KC', 'TS', 'TD' ]))
#6
print(rank_hand([ 'JD', 'KD', 'TD', 'QD', 'AD' ]))
#9

print(rank_hand([ '3D', '4D', '5D', '6D', '7D' ]))
print('----------------------')
print(rank_hand([ '3D', 'JD', '3D', '3D', '3H' ]))
print('----------------------')
print(rank_hand([ 'AD', 'JD', '3D', '3D', '3H' ]))
print('----------------------')
print(rank_hand([ 'AD', 'KS', 'AC', '3D', '3H' ]))
print('----------------------')
print(rank_hand([ 'AD', 'KS', 'AC', '3D', '3H' ]))
print('----------------------')
print(rank_hand([ '3D', '2H', '3C', 'QS', '8D' ]))
print('----------------------')
print(rank_hand([ '3D', '2D', '3D', 'QD', '8D' ]))
print('----------------------')
print(rank_hand([ '3D', '4D', '5D', '6D', '7H' ]))
print('----------------------')
print(rank_hand([ 'TD', 'JD', 'QD', 'KD', 'AD' ]))
print('----------------------')
print(rank_hand([ '3C', 'JD', '4D', '3D', '6H' ]))
print('----------------------')
print(rank_hand([ '3C', 'JD', '4D', '9D', '6H' ]))


#(['7H', '7C', '5D', '7S', '6C'],)

#Difference between obtained and expected output

#Obtained output 4
#Expected output 3

r = rank_hand(['7H', '7C', '5D', '7S', '6C'])
print(r)