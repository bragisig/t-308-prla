tokens = ['+', '>', '*']

def zen_expand(str):
	html = ""
	tag = ""

	if str == '':
		return ''

	for i, c in enumerate(str[::]):
		if c in tokens:
			if c == '>':
				if tag != '':
					html += '<{0}>'.format(tag)

				html += zen_expand(str[i+1:])

				if tag != '':
					html += '</{0}>'.format(tag)

				break
			elif c == '*':
				offset = 1
				while (i+offset < len(str)-1) and (str[i+offset+1].isdigit()):
					offset += 1

				number_of_tags = int(str[i+1:i+1+offset])
				
				next_token = str[i+1+offset:i+1+offset+1]

				counter = 0
				while counter < number_of_tags:
					if tag != '':
						html += '<{0}>'.format(tag)

					next = str[i+2:]
					if next != '':
						html += zen_expand(next)
					
					counter += 1
					
					if tag != '':
						html += '</{0}>'.format(tag)
					
				break
			elif c == '+':
				if tag != '':
					html += '<{0}></{0}>'.format(tag)

			tag = ""

		elif c.isdigit():
			continue
		elif i == len(str)-1:
			tag += c
			if c != '':
				html += '<{0}></{0}>'.format(tag)
		else:
			if c != '':
				tag += c

	return html

h5 = zen_expand("p*2+ul")
print(h5)