from insert_operators import insert_operators

print(insert_operators([14,8,2,17,5,9],83))
#"14+82-17-5+9=83"
print(insert_operators([34,9,82,21,32],32850))
#"34982-2132=32850"
print(insert_operators([1,2,3],5))
#None

#n = [14,8,2,17,5,9]
#o = ('+', '', '-', '-', '+')