from itertools import *

def insert_operators(eqn, target):
	operators = list(product(['+', '-', ''], repeat=len(eqn)-1))
	numbers = [str(n) for n in eqn]

	for operators_comp in operators:
		l = list(chain(*zip_longest(numbers,operators_comp, fillvalue='')))
		equation = ''.join(l)
		result = eval(equation)

		if result == target:
			return str(equation + '=' + str(result))

	return None

