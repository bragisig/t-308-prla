from zen_expand import zen_expand

#h1 = zen_expand("a+div+p*3>div>p")
#print('Result 1: ', h1)

h2 = zen_expand("body>p+table>tr*2>td*3")
print('Result 2: ', h2)

h3 = zen_expand("a+div+p*3")
print('Result 3: ', h3)

h4 = zen_expand("ul>li*10")
print('Result 4', h4)

h5 = zen_expand("div*3>p*2+ul")
print('Result 5', h5)

#div*3>p*2+ul>li*4>a+p*2>a

#"<a></a><div></div><p></p><p></p><p></p>"

#"<table><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>"

#fragments = "a+div+p*3>div>p"
#for token in tokens:
#    fragments = join(f.split(token) for f in fragments)

#print(fragments)

#"<a></a><div></div><p></p><p></p><p></p>"
#>>> zen_expand("dd")
#"<dd></dd>"
#>>> zen_expand("table>tr*3>td*2")
#"<table><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr></table>"