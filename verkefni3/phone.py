import urllib.request as q, re

def phone(p):
	u = 'http://ja.is/?q2=&q={0}'.format(p)
	r = q.urlopen(u)
	h = r.read().decode()

	f = re.findall(r'<span class="cut">(.*?)(<i>(.*?)</i>){0,1}</span>', h)
	l = []
	for e in f:
		l.append((e[0].strip(), e[2] or None))

	return l

r = phone('5541614')
print(r)