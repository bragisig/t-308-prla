import re

def css_properties(css):
    r = [(x[0].strip(), x[1].strip()) for x in re.findall('([\w|-]+[\s]{0,}):([^;]+)', css)]
    return r