import re, os
from operator import itemgetter

def parse_submissions(o):
	m = []
	
	for r, d, fi in os.walk(o):
		for f in fi:
			if f.endswith('.tcl'):
				print(r, f)
				fp = os.path.join(r, f)
				with open(fp) as t:
					c = t.read()
					e = re.findall('Date\s(.+)|Problem\s(.+)|Team\s(.+)|Classify\s(.+)', c)

					l = []
					for w in e:
						cr = list(filter(None, w))
						l.append(cr[0].replace('{', '').replace('}',''))

					if l[3] == 'Accepted':
						m.append(l)

	m = sorted(m, key=itemgetter(0))

	l = []
	for r in m:
		t = (r[2], r[1])
		l.append(t)

	return l
