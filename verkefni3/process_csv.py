import csv

def process_csv(f):
    with open(f, newline='', encoding='utf8') as v:
        r = csv.reader(v)

        d = {}
        for w in r:
            a = float(w[2])*float(w[3])
            if w[0] in d:
                d[w[0]] += a
            else:
                d[w[0]] = a

        return d