from jam import jam


r = jam("""1/1/1 22 December 1967, Nicholas Parsons with Derek Nimmo, Clement Freud, Wilma Ewart and Beryl Reid, excuses for being late.
2/1/2 29 December 1967, Nicholas Parsons with Derek Nimmo, Clement Freud, Sheila Hancock and Carol Binstead, bedrooms.
3/1/3 5 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Betty Marsden and Elisabeth Beresford, ?
4/1/4 12 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Isobel Barnett and Bettine Le Beau, ?
5/1/5 20 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Prunella Scales, the brownies
6/1/6 27 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Marjorie Proops and Millie Small, ?
7/1/7 2 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Aimi Macdonald and Una Stubbs, my honeymoon.
8/1/8 9 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Lucy Bartlett and Anona Winn, bloomer.
9/1/9 17 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Charmian Innes, ?
10/1/10 23 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Barbara Blake and Renee Houston, my first grown-up dress.
11/1/11 1 March 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Eleanor Summerfield, ?
12/1/12 8 March 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Betty Marsden, ?
13/1/13 15 March 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Charmian Innes, when I wear a top hat.
14/1/14 22 March 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Betty Marsden, keeping wicket.
15/1/15 29 March 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Charmian Innes, outdoor games.
16/1/16 5 April 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Aimi Macdonald, how to stay lovely.
17/2/1 30 September 1968, Nicholas Parsons with Kenneth Williams, Clement Freud and Geraldine Jones, learning to fly.
18/2/2 7 October 1968, Clement Freud with Kenneth Williams, Nicholas Parsons and Geraldine Jones, making an entrance.
19/2/3 14 October 1968, Geraldine Jones with Kenneth Williams, Clement Freud and Nicholas Parsons, how to eat macaroni delicately and without cutting it.
20/2/4 21 October 1968, Kenneth Williams with Clement Freud, Nicholas Parsons and Geraldine Jones, the days of the week.
21/2/5 28 October 1968, Nicholas Parsons with Kenneth Williams, Clement Freud and Geraldine Jones, winter woollies.
22/2/6 4 November 1968, Nicholas Parsons with Kenneth Williams, Clement Freud and Geraldine Jones, Pythagoras.
23/3/1 31 December 1968, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, ?
24/3/2 7 January 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, how to get a seat in a crowded train.
25/3/3 14 January 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, doing nothing.
26/3/4 21 January 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, what you see in the looking glass.
27/3/5 28 January 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Betty Marsden, striking a fresh acquaintance.
28/3/6 4 February 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Lucy Bartlett, dog watch.
29/3/7 11 February 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, giving up smoking.
30/3/8 18 February 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, the best hour of the week.
31/3/9 25 February 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, gossipping.
32/3/10 4 March 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, what to do with your vitality.
33/3/11 11 March 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Aimi Macdonald, how to be good.
34/3/12 18 March 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Juno Alexander, ?
35/3/13 25 March 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, how best to colonise the Moon.
36/3/14 1 April 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, ?
37/4/1 29 September 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, what to reply to how to you do.
38/4/2 6 October 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Miriam Karlin, where to draw the line.
39/4/3 13 October 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, preventing long telephone conversations.
40/4/4 20 October 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Juno Alexander, the function of a good waiter.
41/4/5 27 October 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Teddie Beverley, the things I say.
42/4/6 3 November 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Sheila Hancock, bloomers.
43/4/7 10 November 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Geraldine Jones, interviewing a secretary.
44/4/8 17 November 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Fenella Fielding, receiving compliments.
45/4/9 24 November 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Sheila Hancock, appearing in public.
46/4/10 1 December 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Andree Melly, what I most desire.
47/4/11 8 December 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Sheila Hancock, watchdog.
48/4/12 15 December 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Moira Lister, hot dogs.
49/4/13 22 December 1969, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Sheila Hancock, joy.
""")

print(r)


r2 = jam("""1/1/1 22 December 1967, Nicholas Parsons with Derek Nimmo, Clement Freud, Wilma Ewart and Beryl Reid, excuses for being late.
2/1/2 29 December 1967, Nicholas Parsons with Derek Nimmo, Clement Freud, Sheila Hancock and Carol Binstead, bedrooms.
3/1/3 5 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Betty Marsden and Elisabeth Beresford, ?
4/1/4 12 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Isobel Barnett and Bettine Le Beau, ?
5/1/5 20 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Prunella Scales, the brownies
230/11/11 12 January 1977, Ian Messiter with Kenneth Williams, Derek Nimmo, Peter Jones and Nicholas Parsons, bangs.
6/1/6 27 January 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Marjorie Proops and Millie Small, ?
7/1/7 2 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Aimi Macdonald and Una Stubbs, my honeymoon.
8/1/8 9 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Lucy Bartlett and Anona Winn, bloomer.
9/1/9 17 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Andree Melly and Charmian Innes, ?
10/1/10 23 February 1968, Nicholas Parsons with Derek Nimmo, Clement Freud, Barbara Blake and Renee Houston, my first grown-up dress.
Tony Hawks, Sheila Hancock, Tim Rice, Wendy Richard, Stephen Fry, Barry Cryer, Richard Murdoch, Peter Cook, Ian Messiter, Victoria Wood, Jimmy Mulville, Elaine Stritch and Sandi Toksvig, Silver Minutes.
245/11/26 27 April 1977, Nicholas Parsons with Kenneth Williams, Derek Nimmo, Clement Freud and Peter Jones, plus Ian Messiter, my dignity.""")

print(r2)



