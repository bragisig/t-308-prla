import xml.etree.ElementTree as et
import math as m

def hungry(p, la1, lo1):
	d = 0
	t = et.parse(p)
	o = t.getroot()
	for c in o:
		g = './tag/'
		c1 = c.find(g+'[@k="name"]')
		c2 = c.find(g+'[@v="restaurant"]')
		c3 = c.find(g+'[@v="fast_food"]')
		if c1 != None and (c2 != None or c3 != None):
			la2 = float(c.get('lat'))
			lo2 = float(c.get('lon'))
			r = m.sqrt((la2-la1)**2+(lo2-lo1)**2)
			if d == 0 or d > r:
				d = r
				re = c1.get('v')

	return re

