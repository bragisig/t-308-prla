def jam(s):
	d = {}

	def sf(r, w):
		l = []
		for f in r:
			for n in f.split(w):
				l.append(n.strip())

		return l
	
	ln = s.splitlines()
	for r in ln:
		n = r.split(',')
		n = n[1:-1]
		n = sf(n, ' plus ')
		n = sf(n, ' with ')
		n = sf(n, ' and ')
		n = list(filter(None, n))

		for f in n:
			if f in d:
				d[f] += 1
			else:
				d[f] = 1

	return d