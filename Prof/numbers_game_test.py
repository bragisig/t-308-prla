
from numbers_game import numbers_game

print(numbers_game([8,3,29,12,7,1,20,39]))
#True
print(numbers_game([3,2,4,6,3,3,15,3]))
#False

#To the right of each even number must be either be an odd number, or the same even number.
#To the right of each odd number must be either a larger even number or a strictly smaller odd number.
