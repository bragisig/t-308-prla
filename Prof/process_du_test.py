from process_du import process_du


r = process_du("""228136 ./Example preim 312
202099  ./Library
561775  ./Submission-arXiv v1
100294  ./B-inv-proposition
799927  ./CodeSage/SageRuns
826594  ./CodeSage
569863  ./Submission-FPSAC2012/Final submission
315957  ./Submission-FPSAC2012/Sample
1210768 ./Submission-FPSAC2012/Galley
6670    ./Submission-FPSAC2012/Original submission/auto
572082  ./Submission-FPSAC2012/Original submission
2737508 ./Submission-FPSAC2012
1298628 ./CodeHaskell/s
4513852 ./CodeHaskell
9759720 .""")

print(r)


#['.', 'CodeHaskell', 'Submission-FPSAC2012', 's', 'Galley', 'CodeSage', 'SageRuns',
#'Original submission', 'Final submission', 'Submission-arXiv v1', 'Sample',
#'Example preim 312', 'Library', 'B-inv-proposition', 'auto']

r = process_du("""228136  Meshmachine/Example preim 312
202099  Meshmachine/Library
561775  Meshmachine/Submission-arXiv v1
100294  Meshmachine/B-inv-proposition
799927  Meshmachine/CodeSage/SageRuns
826594  Meshmachine/CodeSage
569863  Meshmachine/Submission-FPSAC2012/Final submission
315957  Meshmachine/Submission-FPSAC2012/Sample
1210768 Meshmachine/Submission-FPSAC2012/Galley
6670    Meshmachine/Submission-FPSAC2012/Original submission/auto
572082  Meshmachine/Submission-FPSAC2012/Original submission
2737508 Meshmachine/Submission-FPSAC2012
1298628 Meshmachine/CodeHaskell/s
4513852 Meshmachine/CodeHaskell
9759720 Meshmachine""",)

print(r)