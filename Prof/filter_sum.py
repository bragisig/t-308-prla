def filter_sum(lis):
	r = [x for x in lis if x > 10 and x < 95]
	s = sum(r)

	return (s, r)
