import re

def spell_check(path_to_file, s):
	word_list = []
	with open(path_to_file, newline='', encoding='utf8') as f:
		for word in f:
			word_list.append(word.upper().rstrip())

	words_to_check = re.findall('[a-zA-Z0-9_-]+', s)

	misspelled = []

	for word_to_check in words_to_check:
		correct = False
		w1 = word_to_check.upper()
		for word in word_list:
			if w1 == word:
				correct = True 
				break

		if correct == False:
			misspelled.append(word_to_check)

	return misspelled


