import itertools

def numbers_game(l):
	for i in l:
		if i > 100 or i < 1:
			return False

	counter = 0

	p = itertools.permutations(l, len(l))
	for possibility in p:
		legal = True
		for i in range(len(l)-1):
			if possibility[i] % 2 == 0:
				if not (possibility[i+1] % 2 != 0 or possibility[i+1] == possibility[i]):
					legal = False
					break
			else:
				if not ((possibility[i+1] > possibility[i] and possibility[i+1] % 2 == 0) or (possibility[i+1] < possibility[i] and possibility[i+1] % 2 != 0)):
					legal = False
					break
		
		if legal:
			#print(possibility)
			return True

	return legal



