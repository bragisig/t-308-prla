import re

def process_du(output):
	result = []
	for line in output.splitlines():
		found = re.findall('(\d+)\s(.+)', line)
		size = found[0][0]
		folder = found[0][1]
		folder = folder.split('/') 
		t = (int(size), folder[len(folder)-1].lstrip())
		result.append(t)

	ls = sorted(result, key=lambda x: x[0], reverse=True)
	result = [x[1] for x in ls]
	return result
