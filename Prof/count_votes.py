import csv

def count_votes(path_to_csv):
	fields = {}
	with open(path_to_csv, newline='', encoding='utf8') as v:
		reader = csv.reader(v)

		firstline = True
		for line in reader:
			if firstline:
				firstline = False
				continue

			keys = [x.strip() for x in line[3].split(',')]
			keys = list(filter(None, keys))

			for k in keys:
				if k in fields:
					fields[k] += 1
				else:
					fields[k] = 1

	return fields
