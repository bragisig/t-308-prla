-------------------------------------------
ATH! Þarf stagger module til þess að keyra.
-------------------------------------------

usage: ipodpiracy.py [-h] [--clean] source_folder dest_folder

Tidy up your pirated music.

positional arguments:
  source_folder  The folder where the messy music is located
  dest_folder    The new folder with all the well aranged music

optional arguments:
  -h, --help     show this help message and exit
  --clean        If there is data in the destination folder, it will be
                 deleted

Examples:

python3 ipodpiracy.py ipod/ ipod-aranged/

python3 ipodpiracy.py --clean ipod/ ipod-aranged/