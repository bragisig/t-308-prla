usage: exchange_words.py [-h]
                         csv_word_file_path source_text_file dest_text_file
                         old_word new_word

Switches words in a text file

positional arguments:
  csv_word_file_path  The csv containing the dictionary
  source_text_file    The textfile containing the source text
  dest_text_file      The textfile containing the text with the switched words
  old_word            The word to replace
  new_word            The new word replacing the old word

optional arguments:
  -h, --help          show this help message and exit

Examples:
python3 exchange_words.py "exchange/SHsnid.csv"  "exchange/demo_text.txt"  "exchange/demo_text_changed.txt"  "appelsína"  "súpa"

python3 exchange_words.py "exchange/SHsnid.csv"  "exchange/demo_text.txt"  "exchange/demo_text_changed.txt"  "bíll"  "lyftari"