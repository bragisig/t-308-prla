import csv, string, re
import sys, argparse
  
parser = argparse.ArgumentParser(description='Switches words in a text file')
parser.add_argument('csv_word_file_path', metavar='csv_word_file_path', type=str, 
                   help='The csv containing the dictionary')

parser.add_argument('source_text_file', metavar='source_text_file', type=str, 
                   help='The textfile containing the source text')

parser.add_argument('dest_text_file', metavar='dest_text_file', type=str, 
                   help='The textfile containing the text with the switched words')

parser.add_argument('old_word', metavar='old_word', type=str, 
                   help='The word to replace')

parser.add_argument('new_word', metavar='new_word', type=str, 
                   help='The new word replacing the old word')

args  = parser.parse_args()

def exhange_words(csv_word_file_path, text_file_path, new_text_file_path, old_word, new_word):
	def clean_text(text):
		for c in [',', ';', '.', '?', '"', '!']:
			text = text.replace(c, '')
		
		return text

	print('Replacing words...')

	with open(csv_word_file_path, newline='', encoding='utf8') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=';')

		old_word_found = False
		new_word_found = False

		old_word_list = []
		new_word_dict = {}

		#Find the information for the old and new word
		for line in csv_reader:
			if old_word == line[0]:
				old_word_list.append((line[4], line[5]))
        		
				if not old_word_found:
					old_word_found = True

			elif new_word == line[0]:
				new_word_dict[line[5]] = line[4] 
        		
				if not new_word_found:
					new_word_found = True
			elif old_word_found and new_word_found:
				break

	try:
		new_text_file = open(new_text_file_path, 'w')
		old_text_file = open(text_file_path) 

		#Loop through the textfile and replace
		for line in old_text_file:
			t = clean_text(line)
			for word in t.split():
				for old in old_word_list:
					if word == old[0]:
						new_word = new_word_dict[old[1]]
						print(word, ' -> ', new_word)
						line = re.sub(r'\b{0}\b'.format(word), new_word, line)
						break

			new_text_file.write(line)

		print('New file with replaced words: ', new_text_file_path)
	except Exception as ex:
		print('Error: ', ex)
	finally:
		new_text_file.close()
		old_text_file.close()
		
		#print(old_word_list)
		#print(new_word_dict)
		#print(line)
        #breyttur = line.replace()

exhange_words(args.csv_word_file_path, args.source_text_file, args.dest_text_file, args.old_word, args.new_word)

#exhange_words("exchange/SHsnid.csv", "exchange/demo_text.txt", "appelsína", "súpa")
#exhange_words("exchange/SHsnid.csv", "exchange/demo_text.txt", "exchange/demo_text_changed.txt", "bíll", "rúta")



