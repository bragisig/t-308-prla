import stagger, os, shutil
import sys, argparse
  
parser = argparse.ArgumentParser(description='Tidy up your pirated music.')
parser.add_argument('source_folder', metavar='source_folder', type=str, 
                   help='The folder where the messy music is located')
parser.add_argument('dest_folder', metavar='dest_folder', type=str, 
                   help='The new folder with all the well aranged music')
parser.add_argument('--clean', action='store_true', default=False,
                   help='If there is data in the destination folder, it will be deleted')

args  = parser.parse_args()

source_folder = args.source_folder
dest_folder = args.dest_folder
no_tags_dir = os.path.join(dest_folder, "NoTags/")

def tidy_pirated_stuff(source_folder, dest_folder, no_tags_dir):
	if args.clean == True:
		shutil.rmtree(dest_folder)

	for root, dirs, files in os.walk(source_folder):
		for f in files:
			try:
				file_name, file_ext = os.path.splitext(f)

				tag = stagger.read_tag(os.path.join(root, f))  

				if tag.title.endswith(file_ext):
					tag.title = tag.title[:-len(file_ext)]

				tag.title = tag.title.replace(r'/', '-')

				if tag.artist == '':
					tag.artist = 'UnknowArtist'

				tag.album = tag.album.replace(r'/', '-')

				new_dir = os.path.join(dest_folder, tag.artist)
				new_dir = os.path.join(new_dir, tag.album)
				if not os.path.exists(new_dir):
					os.makedirs(new_dir)

				new_file = '{0} - {1}{2}'.format(tag.track, tag.title, file_ext)

				shutil.copy(os.path.join(root, f), os.path.join(new_dir, new_file))

				#print('Copied file: ', new_file)
			except stagger.errors.NoTagError:
				#print('NoTagError! File copied to "NoTags" folder: ', f)

				if not os.path.exists(no_tags_dir):
					os.makedirs(no_tags_dir)

				shutil.copyfile(os.path.join(root, f), os.path.join(no_tags_dir, f))
			except:
				print('Error! File: ', f)

	print('Done! Check your destination folder: ', dest_folder)


tidy_pirated_stuff(source_folder, dest_folder, no_tags_dir)

