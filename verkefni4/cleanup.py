import os, re, shutil
import sys, argparse
  
parser = argparse.ArgumentParser(description='Tidy up your tv shows.')

parser.add_argument('download_folder', metavar='download_folder', type=str, 
                   help='The folder where the downloaded tv shows are located')

parser.add_argument('search_string', metavar='search_string', type=str, 
                   help='Search for these word in the filename')

parser.add_argument('dest_folder', metavar='dest_folder', type=str, 
                   help='The folder where the tv shows will be copied or moved to')

parser.add_argument('--search_start', action='store_true', default=False,
                   help='Search string must match the start of the string')

parser.add_argument('--clean_first', action='store_true', default=False,
                   help='Clean the download folder of unwanted files (.txt, .nfo, .dat)')

parser.add_argument('--move_files', action='store_true', default=False,
                   help='Move the matched files instead of copying them')

parser.add_argument('--test', action='store_true', default=False,
                   help='Do a test run to see which files will be moved or copied')

args  = parser.parse_args()

def tidy_download(download_folder, search_string, dest_folder, search_start_of_string_only=False, clean_first=False, move_files=False, test_run=False):
	def clean_download(download_folder, extensions=['.txt', '.nfo', '.dat'], test_run=False):
		delete_count = 0
		for root, dirs, files in os.walk(download_folder):
			for filename in files:
				file_name, file_ext = os.path.splitext(filename)
				if file_ext in extensions:
					if test_run:
						print('Will delete', os.path.join(root, filename))
					else:
						os.remove(os.path.join(root, filename))
						print('Deleted', os.path.join(root, filename))

					delete_count += 1

		return delete_count

	def get_season(filename):
		found = re.search('[S|s]([\d]{1,2})|(\d{1,2})x\d{1,2}', filename)
		if found:
			return 'Season ' + (found.group(1) or found.group(2))
		else:
			return ''

	delete_count = 0
	if clean_first:
		delete_count = clean_download(download_folder, test_run=test_run)

	find_name_regex = r'[\s.]'.join(w.upper() for w in search_string.split())

	count = 0
	for root, dirs, files in os.walk(download_folder):
			for filename in files:
				try:
					filename_uppercase = filename.upper()
					if search_start_of_string_only:
						found = re.match(find_name_regex, filename_uppercase)
					else:
						found = re.search(find_name_regex, filename_uppercase)
					
					if found:
						season = get_season(filename)
						if season:
							print(filename, '({0})'.format(season))
						else:
							print(filename)

						season_dest_folder = os.path.join(dest_folder, season)

						if not test_run:
							if not os.path.exists(season_dest_folder):
								os.makedirs(season_dest_folder)

							if move_files:
								shutil.move(os.path.join(root, filename), season_dest_folder)
							else:
								shutil.copy(os.path.join(root, filename), season_dest_folder)

							if not os.listdir(root):
								os.rmdir(root)

						count += 1

				except Exception as e:
					raise e
				finally:
					pass

	if test_run:
		if clean_first:
			print('Cleaning test is done! Will delete', delete_count, 'files')	
			
		print('Found', count, 'files to be moved or copied - Running in test mode, no files moved or copied')
	else:
		if clean_first:
			print('Cleaning is done! Deleted', delete_count, 'files')

		if move_files:
			print('Found and moved', count, 'files')
		else:
			print('Found and copied', count, 'files')


tidy_download(args.download_folder, args.search_string, args.dest_folder, args.search_start, args.clean_first, args.move_files, args.test)


