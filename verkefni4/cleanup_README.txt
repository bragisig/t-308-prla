usage: cleanup.py [-h] [--search_start] [--clean_first] [--move_files]
                  [--test]
                  download_folder search_string dest_folder

Tidy up your tv shows.

positional arguments:
  download_folder  The folder where the downloaded tv shows are located
  search_string    Search for these word in the filename
  dest_folder      The folder where the tv shows will be copied or moved to

optional arguments:
  -h, --help       show this help message and exit
  --search_start   Search string must match the start of the string
  --clean_first    Clean the download folder of unwanted files (.txt, .nfo,
                   .dat)
  --move_files     Move the matched files instead of copying them
  --test           Do a test run to see which files will be moved or copied

Examples:

python3 cleanup.py --clean_first "cleanup/" "dragons den" "Shows/Dragons Den/"

python3 cleanup.py --test "cleanup/" "mad men" "Shows/Mad Men/" 

python3 cleanup.py --clean_first --move_files "cleanup/" "8 out of 10" "Shows/8 out of 10 Cats/" 