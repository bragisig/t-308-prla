from operator import mul as o
from datetime import date as d
from builtins import int as i, list as l, sum as u, map as p

y = { '0': '20', '9': '19'}
m = [3,2,7,6,5,4,3,2]

def valid(n):
	try:
		c=-1
		d(i(y[n[9]]+n[4:6]), i(n[2:4]), i(n[0:2]))
		
		s = u(p(o, m, [i(x) for x in l(n[:-2])]))

		c = 11-(s%11)
	finally:
		return c == i(n[8])
