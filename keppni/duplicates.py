
from collections import Counter as c

def duplicates(l):
	return [i for i in c(l) if c(l)[i]>1]