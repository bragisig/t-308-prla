from builtins import range as r , min as s, len as e

def longest_common_substring(l):
	m = s(l, key=e)
	l.remove(m)
	n = e(m)


	for i in r(n):
		for j in r(i+1):
			k = 0
			for w in l:
				if m[j:n-i+j] in w:
					k = n-i
				else:
					k = 0
					break

			if k:
				return k

	return 0