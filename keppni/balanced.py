def balanced(s):
	i = 0
	for c in s:
		if i < 0:
			break

		if c == '(':
			i += 1
		else:
			i -= 1

	return i == 0
