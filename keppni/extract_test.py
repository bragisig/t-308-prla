from extract import extract

print( extract('10SM8') )
print( extract('l0-S,eM8') )
print( extract('1-089410') )

print( extract('10s M8') )
#[ '10', 'S', 'M', '8' ]
print( extract('1 0894l0') )
#[ '10', '8', '9', '4', '10' ]
print( extract('5.4-9') )
#[ '5', '4', '9' ]
print( extract('398 4') )
#None

'''
Extract grades

On a graduation diploma, the grades for a given subject are given as the table below shows.

Námsgrein	 Áfangar	 Einkunnir	 Einingar
…	…	…	…
Efnafræði	EFN 1036 2036 3036 4036 4136  	M 10 8 7 S    	18
…	…	…	…
Suppose you are given a text files that contain the result of passing diplomas through an OCR engine. 
OCR engines take an image, containing text, and try to convert it to a text document. 
This process can be inaccurate. E.g., it is can be hard for OCR engines to recognize the difference between 1 and l, O and 0, and so forth.

In this problem, we assume we have been able to extract the part of each line that contains the grades. 
The only grades that can appear on a diploma are the integers from 4 to 10 (inclusive) and the letters S and M. 
The grade string you obtain, however, is not necessarily properly formatted. Spaces may have been removed or added, 
0 could have been read as O, 1 as l and S as s. Furhtermore, the string could contain ‘noise’, such as hyphens, dots and commas.

Write a function extract that takes a string of grades, from an OCR engine, and returns a list of strings containing 
the grades of the string. However, if the string contains illegal characters, or it is impossible to split the 
string up into grades (with the noise removed, and all misread characters corrected), the function should return None.

Example

>>> extract('10s M8')
[ '10', 'S', 'M', '8' ]
>>> extract('1 0894l0')
[ '10', '8', '9', '4', '10' ]
>>> extract('5.4-9')
[ '5', '4', '9' ]
>>> extract('398 4')
None
'''