import re as x


def extract(s):
	s = x.sub('\W', '', s)
	d = { 'L': '1', 'O': '0' }

	for k in d:
		s = s.upper().replace(k, d[k])
	
	r = x.findall('[SM]|10|[4-9]', s)
	if ''.join(r) == s:
		return r

	return None