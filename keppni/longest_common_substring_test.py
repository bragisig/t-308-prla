from longest_common_substring import longest_common_substring


print(longest_common_substring(['lollipop','kiloliters','xylology']))
# 3
print(longest_common_substring(['hello','hello','helloed']))
# 5
print(longest_common_substring(['nooooo','yeeeesss']))
# 0
print(longest_common_substring(['leewards', 'margravates', 'bicarbs', 'darer', 'chowkidar', 'outsmarted', 'wallaroo', 'carfuffles']))
# 2
print(longest_common_substring(['intercoastal', 'coastally', 'pastalike', 'coastally', 'chloroplastal', 'coastal']))
# 5


